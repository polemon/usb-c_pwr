# USB-C Power

USB-C power adapter with 6 pin USB-C connector.

***

## Description

USB power connector with 6 pin USB-C connector for +5V_BUS applications.
